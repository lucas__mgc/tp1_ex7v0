#!/usr/bin/env python
# coding: utf-8

"""

We create a module simplecalculator including a  simple Class SimpleCalculator and try differents methods directlely if module is called directly as a main
"""


class SimpleCalculator:
    """
    This is a class for simple mathematical operations on simple numbers.

    """

    @staticmethod
    def sum(var_a, var_b):
        """
        The function to sum two  Numbers.

        Parameters:
            var_a (int ): First operand
            var_b (int ): Second operand

        Returns:
            (int): A number which contains the sum.
        """
        if isinstance(var_a,int) and isinstance(var_b,int):
            return var_a + var_b
        else:
            return "ERROR"

    @staticmethod
    def substract(var_a, var_b):
        """
        The function to substract two  Numbers.

        Parameters:
            var_a (int ): First operand
            var_b (int ): Second operand

        Returns:
            (int): A number which contains the product.
        """
        if isinstance(var_a,int) and isinstance(var_b,int):
            return var_a - var_b
        else:
            return "ERROR"

    @staticmethod
    def multiply(var_a, var_b):
        """
        The function to multiply two  Numbers.

        Parameters:
            var_a (int ): First operand
            var_b (int ): Second operand

        Returns:
            (int): A number which contains the product.
        """

        if isinstance(var_a,int) and isinstance(var_b,int):
            return var_a * var_b
        else:
            return "ERROR"

    @staticmethod
    def divide(var_a, var_b):
        """
        The function to multiply two  Numbers.

        Parameters:
            var_a (int ): First operand
            var_b (int ): Second operand

        Returns:
            (real): A complex number which contains the result of division
        """
        if isinstance(var_a,int) and isinstance(var_b,int):
            if var_b == 0:
                raise ZeroDivisionError("Cannot divid by zero")
            else:
                return var_a / var_b
        else:
            return "ERROR"


if __name__ == "__main__":
    """
    Simple test in case of main call, 

    """

    VAL_A = 5
    VAL_B = 7
    MY_CALCULATOR = SimpleCalculator()
    print("We will test  5+7, 5-7, 5*7 et 5/7")
    print(MY_CALCULATOR.sum(VAL_A, VAL_B))
    print(MY_CALCULATOR.substract(VAL_A, VAL_B))
    print(MY_CALCULATOR.multiply(VAL_A, VAL_B))
    print(MY_CALCULATOR.divide(VAL_A, VAL_B))
